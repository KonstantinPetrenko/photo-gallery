package petrenkokonstantin.com.photogallery;

import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ImageAdapter extends RecyclerView.
        Adapter<ImageAdapter.ViewHolder> {

    private final String TAG = ImageAdapter.class.getName();
    private File imagesFile;
    private ImageView imagePreview;
    private ScaleAnimation anim;

    public ImageAdapter(File folderFile,
                        ImageView imagePreview) {
        imagesFile = folderFile;
        this.imagePreview = imagePreview;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gallery_images_relative_layout,
                        parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final File imageFile = imagesFile.listFiles()[position];
        ExecutorService service = Executors.newCachedThreadPool();
        service.submit(() -> holder.getImageView().
                setImageBitmap(BitmapWorkerTask.getInstance().showImage(imageFile)));

        holder.itemView.setOnClickListener(v -> {

            DecoderBitmap decoderBitmap = new DecoderBitmap(imagePreview);

                if(decoderBitmap.getStatus().equals(AsyncTask.Status.FINISHED) ||
                        decoderBitmap.getStatus().equals(AsyncTask.Status.PENDING))
                    decoderBitmap.execute(String.valueOf(imageFile));

        });
        setAnimation(holder.itemView);
    }

    @Override
    public int getItemCount() {
        return imagesFile.listFiles().length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;

        public ViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.imageGalleryView);
        }

        public ImageView getImageView() {
            return imageView;
        }
    }

    private void setAnimation(View viewToAnimate) {
        anim = new ScaleAnimation(
                0.0f, 1.0f, 0.0f, 1.0f,
                Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 1.0f);
        anim.setDuration(200);
        viewToAnimate.startAnimation(anim);
    }
    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        holder.itemView.clearAnimation();
    }
}
