package petrenkokonstantin.com.photogallery;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;


public class DecoderBitmap extends AsyncTask<String,Void,Bitmap>{

    private final  String TAG = DecoderBitmap.class.getName();
    private ImageView imagePreview;
    public DecoderBitmap(ImageView imagePreview) { this.imagePreview = imagePreview;}

    @Override
    protected Bitmap doInBackground(String... params) {

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(params[0], bmOptions);
        Cache.getInstance().setPath(params[0]);
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        imagePreview.setImageBitmap(bitmap);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        Log.d(TAG, "Cancel");
    }
}

