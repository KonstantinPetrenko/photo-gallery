package petrenkokonstantin.com.photogallery;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

import static android.net.Uri.fromFile;
public class CameraMainActivity extends Activity {

    private final  String TAG = CameraMainActivity.class.getName();
    RecyclerView.Adapter imageAdapter;
    private final int ACTIVITY_START_CAMERA_APP = 11;
    public static final String APP_PREFERENCES = "settings";
    private SharedPreferences mSettings;
    private String mImageFileLocation = "";
    private String GALLERY_LOCATION = "image gallery";
    private File mGalleryFolder;
    private ImageView imagePreview;
    private RecyclerView mRecyclerView;
    private Uri uriFile;
    private File photoFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_main);

        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        createImageGallery();

        imagePreview = (ImageView) findViewById(R.id.imageView);

        mRecyclerView = (RecyclerView)
                findViewById(R.id.galleryRecyclerView);
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
            mRecyclerView.setLayoutManager
                    (new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true));

        else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            mRecyclerView.setLayoutManager
                    (new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true));

        imageAdapter =
                new ImageAdapter( mGalleryFolder, imagePreview);
        mRecyclerView.setAdapter(imageAdapter);

        if(mSettings.contains("KEY_PHOTO"))
            setImage( mSettings.getString("KEY_PHOTO", ""));
        if(mSettings.contains("int_key")) {
            int Visibility = mSettings.getInt("int_key", -1);
            if(Visibility == 0)
                mRecyclerView.setVisibility(View.VISIBLE);
            else if(Visibility == 4)
                mRecyclerView.setVisibility(View.INVISIBLE);
        }

        final GestureDetectorCompat onTouch = new GestureDetectorCompat(this, new GestureListener());

        imagePreview.setOnTouchListener((view, event) -> {
            onTouch.onTouchEvent(event);
            return true;
        });
    }

    private void setImage(String path) {
        imagePreview.post(()-> {
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(new File(path).getAbsolutePath(), bmOptions);
            imagePreview.setImageBitmap(bitmap);
        });
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        uriFile = savedInstanceState.getParcelable("outputFileUri");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("outputFileUri", uriFile);

    }

    public void takePhoto(View view) {
        Intent callCameraApplicationIntent = new Intent();
        callCameraApplicationIntent.
                setAction(MediaStore.ACTION_IMAGE_CAPTURE);

        photoFile = createImageFile();
        uriFile = Uri.fromFile(photoFile);

        callCameraApplicationIntent.
                putExtra(MediaStore.EXTRA_OUTPUT, fromFile(photoFile));

        startActivityForResult(callCameraApplicationIntent,
                ACTIVITY_START_CAMERA_APP);
    }

    protected void onActivityResult (int requestCode,
                                     int resultCode, Intent data) {
        if(requestCode == ACTIVITY_START_CAMERA_APP
                && resultCode == RESULT_OK) {

            try {
                Bitmap bitmap =
                        MediaStore.Images.Media.getBitmap(this.getContentResolver(), uriFile);
                Toast.makeText(getApplicationContext(), String.valueOf(uriFile), Toast.LENGTH_LONG).show();
                imagePreview.setImageBitmap(bitmap);

                imageAdapter.notifyDataSetChanged();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            File file = photoFile.getAbsoluteFile();
            boolean deleted = file.delete();
        }
    }

    private void createImageGallery() {
        File storageDirectory = Environment.
                getExternalStoragePublicDirectory
                        (Environment.DIRECTORY_PICTURES);
        mGalleryFolder = new File(storageDirectory, GALLERY_LOCATION);
        if(!mGalleryFolder.exists())
            mGalleryFolder.mkdirs();
    }

    File createImageFile() {

        String timeStamp = String.valueOf(System.currentTimeMillis());

        String imageFileName = "IMAGE_" + timeStamp;

        File image = null;
        try {
            image = File.createTempFile(imageFileName,".jpg",
                    mGalleryFolder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (image != null)
            mImageFileLocation = image.getAbsolutePath();
        Cache.getInstance().setPath(mImageFileLocation);

        return image;
    }
    void onSave (String path, int VISIBILITY){
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("KEY_PHOTO", path);
        editor.putInt("int_key", VISIBILITY);
        editor.apply();
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        Animation animationFadeIn = AnimationUtils
                .loadAnimation(getApplicationContext(), R.anim.fade_in);
        Animation animationFadeOut = AnimationUtils
                .loadAnimation(getApplicationContext(),R.anim.fade_out);
        @Override
        public boolean onFling(
                MotionEvent event
                ,MotionEvent motionEvent
                ,float velocityX
                ,float velocityY) {

            int SWIPE_MIN_DISTANCE = 120;
            int SWIPE_THRESHOLD_VELOCITY = 200;
            if(event.getX() - motionEvent.getX() >
                    SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                return false; // from right to left
            }  else if (motionEvent.getX() - event.getX() >
                    SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                return false; // From left to right
            }

            if(event.getY() - motionEvent.getY() >
                    SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
                if(mRecyclerView.getVisibility() == View.INVISIBLE) {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mRecyclerView.startAnimation(animationFadeIn);
                }
                return false; // down up
            }  else if (motionEvent.getY() - event.getY() >
                    SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
                if(mRecyclerView.getVisibility() == View.VISIBLE) {
                    mRecyclerView.startAnimation(animationFadeOut);
                    mRecyclerView.setVisibility(View.INVISIBLE);
                }
                return false; // top down
            }
            return false;
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onSave(Cache.getInstance().getPath(), mRecyclerView.getVisibility());
        Log.d(TAG, Cache.getInstance().getPath());
    }
}

