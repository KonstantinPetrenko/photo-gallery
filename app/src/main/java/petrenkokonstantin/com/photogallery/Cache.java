package petrenkokonstantin.com.photogallery;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

public class Cache {

    private static Cache instance;
    private LruCache<String, Bitmap> lru;
    private String path = "";

    private Cache() {
        int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        int cacheSize = maxMemory / 10;
        lru = new LruCache<String, Bitmap>(cacheSize){
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    public static Cache getInstance() {

        if (instance == null)
            instance = new Cache();

        return instance;
    }

    public LruCache<String, Bitmap> getLru() {
        return lru;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}