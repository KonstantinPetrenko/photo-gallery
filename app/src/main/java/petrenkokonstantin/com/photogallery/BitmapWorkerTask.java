package petrenkokonstantin.com.photogallery;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.File;

public class BitmapWorkerTask {

    private static BitmapWorkerTask instance;
    private final String TAG = BitmapWorkerTask.class.getName();

    public static BitmapWorkerTask getInstance() {

        if (instance == null)
            instance = new BitmapWorkerTask();

        return instance;
    }

    public Bitmap showImage(File file) {

        Bitmap bitmap;
        if (Cache.getInstance().getLru().get(String.valueOf(file)) != null) {
            bitmap = Cache.getInstance().getLru().get(String.valueOf(file));
            Log.d(TAG, "bitmap from cache");
        } else {
            bitmap = decodeSampledBitmapFromStorage(file);
            if (bitmap != null) {
                Cache.getInstance().getLru().put(String.valueOf(file), bitmap);
                Log.d(TAG, "new Bitmap ");
            }
        }
        return bitmap;
    }

    private int calculateInSampleSize(
            BitmapFactory.Options options) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        int reqWidth = 100;
        int reqHeight = 100;
        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    private Bitmap decodeSampledBitmapFromStorage(File imageFile) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imageFile.getAbsolutePath(), options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(imageFile.getAbsolutePath(), options);
    }
}

